**CSC 490 - Mobile Application Development**

**Assignment 1**

**Summary:** Build an Android application that will display a view of a light bulb and provide the ability to turn the light bulb on and off. How you show that the light bulb is on is up to you, as is the mechanism for switching between the on and off states.  You will provide a document detailing the design decisions that you made along with your rationale for those decisions.

**Due:** Monday, 9/12/2016 at 6:00 PM

**Rubric:** 60 Points (+10 Points Bonus)

1. (15 pts) When I launch the app, then the screen should show a view of the light bulb in the off state.
2. (15 pts) When I use the mechanism to switch to the on state, the view of the light bulb should switch to on.
3. (10 pts) When I rotate the screen, then the view of the light bulb should remain the same as before I rotated the screen.
4. (10 pts) Provide a README.md file in the root of your project that details the design decisions that you make, suggests alternatives that you considered, and explains your reasoning for choosing what you did.
5. (10 pts) Impress me.

**Bonus:**

1. (10 pts) When I toggle the state of the light bulb, the camera&#39;s flash should match the state of the light bulb.

**Submission:** You will submit your project by adding it to your git repository and pushing it to your BitBucket repository (that you have granted me access) by the due date and time.  Please name the project lightbulb.
