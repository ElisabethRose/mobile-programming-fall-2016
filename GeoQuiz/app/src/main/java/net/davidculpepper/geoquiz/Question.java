package net.davidculpepper.geoquiz;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by dculpepper on 9/13/2016.
 */
public class Question implements Serializable {
    private final int text;
    private final boolean isTrue;
    private final UUID id;

    public Question(int text, boolean isTrue, UUID id) {
        this.text = text;
        this.isTrue = isTrue;
        this.id = id;
    }

    public boolean isCorrect(boolean response) {
        return response == isTrue;
    }

    public int getText() {
        return text;
    }

    public UUID getId() {
        return id;
    }
}
