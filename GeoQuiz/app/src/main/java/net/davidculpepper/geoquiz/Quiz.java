package net.davidculpepper.geoquiz;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dculpepper on 9/13/2016.
 */
public class Quiz implements Serializable {
    private final int correctResponse;
    private final int wrongResponse;
    private final List<Question> questions;
    private int currentQuestionIndex;

    public Quiz(int correctResponse, int wrongResponse, List<Question> questions) {
        this.correctResponse = correctResponse;
        this.wrongResponse = wrongResponse;
        this.questions = questions;
    }

    public int getCorrectResponse() {
        return correctResponse;
    }

    public int getWrongResponse() {
        return wrongResponse;
    }

    public Question getCurrentQuestion() {
        return questions.get(currentQuestionIndex);
    }

    public int getQuestionNumber() {
        return currentQuestionIndex + 1;
    }

    public int getTotalNumberOfQuestions() {
        return questions.size();
    }

    public void goToNextQuestion() {
        currentQuestionIndex += 1;
    }

    public void goToPreviousQuestion() {
        currentQuestionIndex -= 1;
    }

    public boolean isFirstQuestion() {
        return currentQuestionIndex == 0;
    }

    public boolean isLastQuestion() {
        return currentQuestionIndex == questions.size() - 1;
    }
}
